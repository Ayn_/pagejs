# PageJS
![min size](https://badgen.net/bundlephobia/min/@ayn/pagejs) ![min zipped](https://badgen.net/bundlephobia/minzip/@ayn/pagejs)

Theme-able HTML markup generator.

## API
Sample usage:
```js
const Page = require('@ayn/pagejs')

let page = new Page({ title: 'Hello World!' })
    .div(_=>{
           _.div('I am a div inside a div.')
           _.h1({ style: 'color: red;' },'Cool, uh?', _=>{
                _.br()
                _.span('I am inside h1, so I am red too!')
           })
    })

let markup = page.getMarkup()
```

### Constructor
Constructor takes a single object used for configuration, here are the default values:
```js
{
    doctype: '<!doctype html>',
    lang: 'en',
    charset: 'utf-8',
    title: 'Untitled paged',
    body: 'No content.',
    theme: null,
    tags: {
        self_closing: [
            'area', 'base', 'br', 'col', 'embed', 'wbr', 'input',
            'img', 'source', 'link', 'meta', 'param', 'hr', 'track'
        ],
        autoclose: false,
    }
}
```

### Methods
There is only one method. 
```js
page.getMarkup()
```
Returns the markup code of the page or in the case of a child element (obtained through a tag callback) a subset of it.

Child elements also contain two properties `el.parent` and `el.siblings`.

You can also process objects directly. 
```js
new Page().getMarkup({
    div: {
        attributes: { style: 'background: purple' },
        content: 'wowza',
        children: []
    }
})
```
## Themes

Plain HTML is useful but not very practical in rapid development.

PageJS supports themes in the form of a JSON object. You can create your own as well as used pre-existing ones.
This makes working with css frameworks even easier than working with them directly.

Here's an example: 
```js
let page = new Page({theme:'boostrap-4', title: 'Admin Panel' })
    .navbar({ class: 'dark_dark' }, _=>{
        _.brand({ href:'website.com' }, 'COMPANY')
        _.toggler()
    })
    .container({ class: 'fluid', style: 'padding: 20px'}, _=>{
        _.breadcrumb(_=>{
            _.item('Home')
            _.item('Library')
        })
        _.jumbotron(_=>{
            _.h1('Hello, world!')
            _.p('Welcome to my page! It was made with JS.')
            _.br()
            _.btn('Learn more')
        })
    })
```
Which results in 
![Bootstrap 4 Theme Example](https://i.ibb.co/ngNTdMp/image.png)

Themes allow for the creation of sudo tags and sudo classes.

You can check out a base theme file [here](https://gitlab.com/Ayn_/pagejs-theme-file) as well as find information on its structure.

## Compiling

You can use the [pagejs-webpack-plugin](https://www.npmjs.com/package/pagejs-webpack-plugin).
To compile `.page.js` files into `.html` if you wish to skip html all together in your projects.

## Sponsors

If you would like to support me  
[![Buy Me A Coffee](https://bmc-cdn.nyc3.digitaloceanspaces.com/BMC-button-images/custom_images/orange_img.png)](https://www.buymeacoffee.com/3dpOMCLaR)  
or feel free to hire me! 

## License
  [MIT](LICENSE)