class PageJS {
    constructor(config = {}) {
        // Default values
        this.config = Object.assign({
            doctype: '<!doctype html>',
            lang: 'en',
            charset: 'utf-8',
            title: 'Untitled paged',
            body: 'No content.',
            theme: null,
            themeScript: true,
            themeStyle: true,
            tags: {
                self_closing: [
                    'area', 'base', 'br', 'col', 'embed', 'wbr', 'input',
                    'img', 'source', 'link', 'meta', 'param', 'hr', 'track'
                ],
                autoclose: false,
            }
        }, config)

        /**
         * TODO:
         * -Updated bootstrap 4 sample image to match npm page size
         * -Finish bootstrap theme (WIP)
         * -Think about maybe implementing custom variables (both in theme and pagejs)
         * ^ Would allow for templating functionality?
         * -Work on webpack plugin to allow suppoting partials?
         */
        
        // Basic html structure
        this.dom={
            html:{attributes:{lang:this.config.lang},children:[
                {head:{children:[
                    {meta:{attributes:{charset:this.config.charset}}},
                    {title:{content:this.config.title}}
                ]}},
                {body:{children:[]}}
        ]}}
        
        // Load theme
        if(this.config.theme) {
            try { 
                let file = require(`./themes/${this.config.theme}.json`) 

                this.config.theme = {
                    info: file.shift(),
                    data: file[0]            
                }

                let link = this.config.theme.info.link

                // Append link for library
                if(link && this.config.themeStyle) {

                    let head = this.dom.html.children[0].head.children
                    
                    if(link.forEach) link.forEach(link=>{
                        head.push({ link })
                    })

                    else head.push({ link })
                }

            }
            catch { console.warn('No theme found.'); this.config.theme=null; }
        }

        // Allow wild card functions
        return new Proxy(this, this)
    }

    get(target, prop) {

        // If it exists return appropriate method
        if(this[prop]) return this[prop]

        // Run our function
        return function(...Args) {

            // Work around node utils
            if(typeof prop == 'symbol') return this

            // defaults
            let attributes = {},
                content = '',
                cb = ()=>{},
                target = (Args[3]) ? Args[3].target : this.dom.html.children[1].body.children,
                children = [],
                parent = (Args[3]) ? Args[3].parent : null
                
            // Adjust first 3 args by type
            for(let [i, arg] of Args.entries()) {
                if(i == 3 ) break                   
                switch(typeof arg) {
                    case 'object':
                        attributes = arg
                        break;
                    case 'string':
                        content = arg
                        break;
                    case 'function':
                        cb = arg
                        break;
                    } 
            }
    
            // New branch
            let obj = {
                [prop]: {
                    attributes,
                    content,
                    children,
                }
            }

            let cb_parent = {
                parent,
                target: obj[prop].children
            }

            // Apply theme (if it matches parent children theme key or theme key)
            if(this.config.theme && (this.config.theme.data[cb_parent.parent] || this.config.theme.data[prop])) {

                // Process method
                let process_obj = (theme_obj) => {

                    let children_index = Object.keys(theme_obj).findIndex(key=>key=='children') 
                    
                    let tag = Object.keys(theme_obj)[(children_index || children_index == -1) ? 0 : 1]

                    // Hold 
                    let theme_attributes = theme_obj[tag].attributes || {}
                    
                    let process_attributes = (theme_attributes) => {

                        for(let key in theme_attributes) {

                            // Allow for multiple classes
                            if(key=='class' && attributes[key]) {

                                let arr = []

                                attributes[key].split(' ').forEach(attribute=>{
                                    // If user attribute value matches theme place holder
                                    if(theme_attributes[key][attribute]) {
                                        arr.push(theme_attributes[key][attribute])                       
                                    } else {
                                        arr.push(theme_attributes[key] + ' ' + attribute)
                                    }
                                }) 
                                
                                attributes[key] = arr.join(' ')

                            // Use theme values
                            } else if(!attributes[key]) {
                                let theme_value = theme_attributes[key]
                                // If not string default to first
                                attributes[key] = (typeof theme_value == 'string')?theme_value:Object.values(theme_value)[0]
                            }    
                        }
                    }

                    process_attributes(theme_attributes)

                    // set parent
                    cb_parent.parent = prop
                    cb_parent.target = theme_obj[tag].include  || []

                    // Replace dummy prop with real
                    prop = tag

                    obj = {
                        [prop]: {
                            attributes,
                            content: theme_obj[tag].content || content,
                            children: cb_parent.target,
                        }
                    }

                    let inherit = false         

                    // Process include
                    let process_include = (tag_obj, target) => {
                        
                        let tag_include = tag_obj.include

                        tag_include.forEach((child)=>{

                            let child_tag = Object.keys(child)[0]

                            // Process @
                            if(child_tag[0] == '@') {
                                child[child_tag.substr(1)] = child[child_tag]
                                delete child[child_tag]
                                child_tag = child_tag.substr(1)
                                inherit = child[child_tag]
                            }

                            child = child[child_tag]

                            child.attributes = child.attributes || {}

                            child.content = child.content || ''

                            child.children = child.children || []

                            if(target) target.push( { [child_tag]: child } )

                            if(child.include) {
                                process_include(child, child.children)
                            }

                            if(inherit) {
                                cb_parent.target = inherit.children
                                inherit = false
                            }
                            
                        })
                    }
                    
                    if(theme_obj[tag].include) process_include(theme_obj[tag])

                }

                // Handle scope
                let parent = this.config.theme.data[cb_parent.parent]
                
                if(parent && parent.children) {

                    let child = parent.children.filter(obj=>obj[prop])[0]
                    
                    if(child && child[prop]) {
                        process_obj(child[prop])
                    } else {
                        let theme_obj = this.config.theme.data[prop]
                                
                        // If match is found continue
                        if(theme_obj) {
                            process_obj(theme_obj)
                        }     
                    }

                } else {

                    let theme_obj = this.config.theme.data[prop]
                                
                    // If match is found continue
                    if(theme_obj) {
                        process_obj(theme_obj)
                    } 

                }
   
            }

            // Append to parent
            if(target.push) target.push(obj)
                else target[prop] = obj

            // Modify cb to append to right object
            cb(new Proxy({}, {
                get:(target, sub_prop)=>{
                    // Prop seems to be symbol when loggin
                    if(typeof sub_prop != 'symbol') {
                        // Preset
                        if(sub_prop=='getMarkup') return ()=>this.getMarkup(obj)
                        else if(sub_prop=='parent') return prop
                        else if(sub_prop=='siblings') return obj[prop].children
                        
                        // Catch all (element)
                        else return (attributes, sub_content, cb)=> 
                            this[sub_prop](attributes, sub_content, cb, cb_parent)
                    }
                    // Perhaps append content to a value so user can place it himself
                    else return () => { return { parent: prop, siblings: obj[prop].children } } 
                }
            }))

            return this

        }   
    }

    getMarkup(obj) {

        if(!obj) {
            
            // Append script tag
            if(this.config.theme && this.config.themeScript) {
                let script = this.config.theme.info.script
                
                if(script) {
                    let body = this.dom.html.children[1].body.children
                
                    if(script.forEach) script.forEach(script=>{
                        body.unshift({ script })
                    })
    
                    else body.unshift({ script })
    
                    this.config.theme.info.script = null
                }
            } 

            obj=this.dom
        }
        
        if(obj.html) var markup = this.config.doctype
            else var markup = ''

        let parse = (obj, content ='') => {

            let markup = ''

            for(let el in obj) {

                let formatted_attributes = ''
                
                // Handle attributes
                if(obj[el].attributes)
                    for(let attribute in obj[el].attributes) 
                        formatted_attributes += `${attribute}='${obj[el].attributes[attribute]}'`
                
                // Handle content
                if(typeof obj[el].content == 'string')   
                    content = obj[el].content

                let inner_markup = ''

                // Handle children
                if(obj[el].children) 
                    if(obj[el].children.forEach) {
                        obj[el].children.forEach(child=>{
                            inner_markup += parse(child, content)
                        })
                    }
                
                // Generate tag
                markup += `<${el}${(formatted_attributes) ? ' '+formatted_attributes:""}${(this.config.tags.autoclose)?" /":""}>${(!this.config.tags.self_closing.includes(el))? content+inner_markup+'</'+el+'>' : ''}`

                return markup
            }

        }

        markup += parse(obj || this.dom)

        return markup
        
    }
}

module.exports = PageJS